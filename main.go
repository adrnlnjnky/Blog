package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func Serve(ctx context.Context, messages <-chan string) {
	r := mux.NewRouter()

	go func() {
		for msg := range messages {
			r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

				// We can pull in dynamic refresh time from the messages herer
				refresh := `<head>
					<link rel="stylesheet" type="text/css" href="https://mygeoangelfirespace.city/styles/style.css">
					<meta http-equiv="refresh" content="0.2">
				</head>`

				w.Write([]byte(refresh + "\n" + msg + "\n"))
			})
		}
	}()

	srv := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:1917",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}

func updater() {
	// 	// all we need to do is call the scrips then sleep for a given period and then
	// 	// do it all over.  s
	// 	1. call the function
	// 	2. sleep
	// 	3. set a forever loop

}

func main() {
	port := "23.73.19.3:4000"

	updater()

	static := http.FileServer(http.Dir("./"))
	http.Handle("/", static)

	fmt.Println("Serving on port:23.73.19.2:4000 ")
	if err := http.ListenAndServe(port, nil); err != nil {
		log.Fatalf("could not listen on port %v. Error: %v", port, err)
	}
}
